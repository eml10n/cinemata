608
01:51:32,541 --> 01:51:36,916
<I>Forensic Architecture asks me to join
<I>an investigation into NSO Group,


609
01:51:37,083 --> 01:51:40,083
<I>an Israeli cyberweapons manufacturer.


610
01:52:00,750 --> 01:52:02,208
<I>Is this recording? No?


611
01:52:02,375 --> 01:52:03,791
<I>Is this recording?


612
01:52:05,666 --> 01:52:06,916
<I>Hi Laura...


613
01:52:07,083 --> 01:52:10,208
<I>So you asked me
<I>to start sending memos about


614
01:52:10,375 --> 01:52:12,583
<I>this ongoing investigation.


615
01:52:16,041 --> 01:52:16,791
<I>So like this...


616
01:52:16,958 --> 01:52:20,625
<I>We were watching the news about
<I>the spread of surveillance technology


617
01:52:20,791 --> 01:52:22,083
<I>for Corona tracing.


618
01:52:22,291 --> 01:52:25,458
<I>So, the way that kind
<I>of global viral emergency


619
01:52:25,625 --> 01:52:27,875
<I>is used to roll out
<I>the surveillance projects


620
01:52:28,041 --> 01:52:30,458
<I>in different parts of the world.


621
01:52:34,416 --> 01:52:37,416
<I>One company name
<I>was always there...


622
01:52:38,333 --> 01:52:40,166
<I>NSO Group.


623
01:52:40,458 --> 01:52:45,458
<I>So, we already wanted for a long time
<I>to start an investigation on the NSO


624
01:52:45,708 --> 01:52:50,625
<I>because NSO software was used
<I>to target our colleagues and friends.


625
01:52:51,541 --> 01:52:54,916
<I>We know that you know
<I>what it feels to be surveilled,


626
01:52:55,083 --> 01:52:58,416
<I>that surveillance is not only
<I>a breach of privacy,


627
01:52:58,583 --> 01:52:59,875
<I>that it is violence,


628
01:53:00,041 --> 01:53:01,791
<I>that it does things to you,


629
01:53:02,125 --> 01:53:05,500
<I>that things start to happen to you
<I>in the real world...


630
01:53:05,750 --> 01:53:08,875
<I>And then it spreads across
<I>your network of friends, colleagues,


631
01:53:09,041 --> 01:53:11,291
<I>and stakeholders or clients...


632
01:53:11,583 --> 01:53:15,250
<I>And that being targeted
<I>feels like terror.


633
01:53:22,166 --> 01:53:27,375
<I>New York City, summer 2020


634
01:53:48,041 --> 01:53:50,333
<I>After their friends and colleagues
<I>are hacked,


635
01:53:50,500 --> 01:53:52,291
<I>the research group Forensic Architecture


636
01:53:52,458 --> 01:53:54,500
<I>begins mapping the global landscape
<I>of digital infections.


637
01:53:54,791 --> 01:53:58,708
If I’m an organization paying
a lot of money for each targeting hack,


638
01:53:58,875 --> 01:54:03,041
the right thing is to target a person
at the center of the network.


639
01:54:03,291 --> 01:54:07,166
Carmen is the head person
of the matrix.


640
01:54:07,416 --> 01:54:10,625
So how can you imagine, Nathan,


641
01:54:10,791 --> 01:54:13,541
a way of visualizing
something like that?


642
01:54:15,041 --> 01:54:18,666
There is a way to illustrate it
through the events.


643
01:54:20,375 --> 01:54:23,291
Anything that's a sphere
is an attack.


644
01:54:23,583 --> 01:54:24,958
It means that


645
01:54:25,250 --> 01:54:30,625
this person on the timeline has been
directly targeted by that act of violence.


646
01:54:30,875 --> 01:54:34,333
So, it might be a digital act of violence or
a physical act of violence,


647
01:54:34,500 --> 01:54:36,333
but it's a direct attack
on that person.


648
01:54:38,625 --> 01:54:41,333
When Carmen was first targeted,


649
01:54:41,500 --> 01:54:43,041
if she clicked on that,


650
01:54:43,208 --> 01:54:45,000
it's quite possible they gained access


651
01:54:45,166 --> 01:54:48,208
to all the conversations she was having
with all of the members of her team.


652
01:54:48,375 --> 01:54:50,791
So, from Carmen, and
they attack everybody else.


653
01:54:50,958 --> 01:54:55,083
And then in 2016, that's when
five subjects raid her office.


654
01:55:01,208 --> 01:55:05,250
<I>Newsroom break-in, Aristegui Noticias.


655
01:55:05,875 --> 01:55:07,291
Hola Carmen!


656
01:55:07,458 --> 01:55:12,625
I am the lead researcher on this
project with Forensic Architecture.


657
01:55:13,375 --> 01:55:17,666
We're focusing specifically
on the targeted digital violence of


658
01:55:17,916 --> 01:55:21,416
the Israeli cyber weapons
manufacturer, NSO Group,


659
01:55:21,583 --> 01:55:24,291
and the use of Pegasus
around the world.


660
01:55:24,666 --> 01:55:26,041
You are one of...


661
01:55:26,208 --> 01:55:30,083
the first persons that was documented
by Citizen Lab as being


662
01:55:30,250 --> 01:55:33,000
targeted using Pegasus.


663
01:55:33,916 --> 01:55:36,583
As a Mexican journalist,


664
01:55:36,750 --> 01:55:41,083
the experience of
having suffered surveillance


665
01:55:41,250 --> 01:55:45,500
by the malware Pegasus
is multifaceted.


666
01:55:45,666 --> 01:55:50,750
It’s a malware that activates
your own phone,


667
01:55:50,916 --> 01:55:53,166
that activates your own camera,


668
01:55:53,333 --> 01:55:54,666
your own microphone.


669
01:55:54,875 --> 01:55:59,333
Can you describe what you remember
as the first message you received?


670
01:55:59,500 --> 01:56:00,791
The hacks?


671
01:56:01,500 --> 01:56:05,166
It was impossible
not to open the links...


672
01:56:05,541 --> 01:56:11,083
For example, a friend notifies you
about the death of someone,


673
01:56:11,250 --> 01:56:12,875
but they are alive.


674
01:56:15,125 --> 01:56:18,041
<I>Dear Carmen, my brother died in
<I>an accident, I’m devastated, I send you


675
01:56:18,208 --> 01:56:21,000
<I>the information about the funeral,
<I>I hope you can come:


676
01:56:21,291 --> 01:56:27,583
Emilio, my son, for example,
received on his phone


677
01:56:28,333 --> 01:56:31,125
a fake news article.


678
01:56:31,750 --> 01:56:35,708
<I>Beheaded journalist is found in Veracruz
<I>with threatening Narco message.


679
01:56:35,875 --> 01:56:37,208
<I>Photos and Details:


680
01:56:37,458 --> 01:56:41,791
A child opens the link
because it is about his mom.


681
01:56:55,625 --> 01:56:58,708
<I>Hi Laura, I hope you're okay...


682
01:56:58,875 --> 01:57:01,708
<I>So last night,
<I>I had horrible dreams.


683
01:57:01,875 --> 01:57:04,541
<I>I dreamt that
<I>I was being interrogated,


684
01:57:04,750 --> 01:57:08,875
<I>and I woke up with extreme
<I>anxiety and in cold sweats,


685
01:57:09,041 --> 01:57:11,791
<I>and the first thought
<I>that came to my mind was,


686
01:57:11,958 --> 01:57:14,083
<I>"Are we making a huge mistake?"


687
01:57:14,250 --> 01:57:16,208
<I>I don’t want to get too paranoid,


688
01:57:16,375 --> 01:57:20,125
<I>but I’m also thinking at the same time
<I>that we’ve never had to come across


689
01:57:20,291 --> 01:57:22,708
<I>a company like the NSO group


690
01:57:22,875 --> 01:57:26,583
<I>They’re very lawsuit happy,
<I>they go after people, they intimidate...


691
01:57:26,750 --> 01:57:28,916
<I>Every single person
<I>we’ve spoken to so far,


692
01:57:29,083 --> 01:57:32,166
<I>whether they’re journalists,
<I>lawyers suing them, researchers,


693
01:57:32,333 --> 01:57:34,416
<I>or people who’ve been targeted...
<I>they’re all telling us


694
01:57:34,625 --> 01:57:38,750
<I>this company tries to intimidate,
<I>to scare, to make your life hell,


695
01:57:38,916 --> 01:57:41,000
<I>just so that you go away.


696
01:57:44,875 --> 01:57:48,375
<I>Nihalsing Rathod,
<I>Human rights lawyer, India.


697
01:57:49,750 --> 01:57:51,208
It is violence...


698
01:57:51,375 --> 01:57:52,625
Violence.


699
01:57:53,000 --> 01:57:56,166
The act is internal,


700
01:57:56,333 --> 01:57:59,541
it is not on the flesh, but it is there.


701
01:57:59,791 --> 01:58:04,791
We have to understand it is
an incredible violence to break a person.


702
01:58:05,833 --> 01:58:07,333
I am not in jail.


703
01:58:07,500 --> 01:58:09,958
I have not been arrested.
So I carry on.


704
01:58:10,583 --> 01:58:15,791
As an Arab, we have interest in
what is going on in the Arab world.


705
01:58:16,125 --> 01:58:21,458
So for us, it's like a personal matter
as well as a legal matter.


706
01:58:21,666 --> 01:58:23,958
This is not the first time
that we've been targeted


707
01:58:24,125 --> 01:58:26,708
or that we were under surveillance
by the Indian state.


708
01:58:26,875 --> 01:58:30,083
A lot of my contacts, when I told them,
look this is happening to me,


709
01:58:30,250 --> 01:58:32,666
They said, "We always presumed
this would happen to you


710
01:58:32,833 --> 01:58:35,208
"so don't worry,
we all prepared for it.


711
01:58:35,375 --> 01:58:38,125
"You must be doing something
really wonderful


712
01:58:38,291 --> 01:58:41,500
"because the government
is spending a lot of money on this."


713
01:58:42,333 --> 01:58:44,166
These are the phones.


714
01:58:44,375 --> 01:58:46,083
<I>Yahya Assiri, Saudi dissident U.K


715
01:58:46,250 --> 01:58:47,625
They're all active?


716
01:58:48,541 --> 01:58:49,416
All active.


717
01:58:49,583 --> 01:58:53,708
In Mexico, when you study journalism,


718
01:58:53,875 --> 01:58:58,250
you take for granted that
the government is going to be after you.


719
01:58:58,500 --> 01:59:00,250
And we are the lucky ones


720
01:59:00,416 --> 01:59:03,291
because in so many regions
in Mexico,


721
01:59:03,458 --> 01:59:05,708
they get a bullet,
they don't get a message...


722
01:59:05,875 --> 01:59:07,208
They get a bullet.


723
01:59:08,041 --> 01:59:14,333
<I>Cybersecurity researchers
<I>at the Citizen Lab


724
01:59:14,500 --> 01:59:21,333
<I>have discovered Pegasus infections
<I>in 45 countries.


725
01:59:26,125 --> 01:59:31,208
<I>FBI delivers tactical gear
<I>to NYPD, June 5


726
01:59:32,208 --> 01:59:35,916
I'm not sure we can make the argument
that what the NSO is enabling,


727
01:59:36,083 --> 01:59:39,791
in terms of operationalizing
its surveillance into direct violence,


728
01:59:39,958 --> 01:59:41,958
is new territory, right?


729
01:59:42,375 --> 01:59:45,666
I mean the CIA and NSA
do that all the time.


730
01:59:46,333 --> 01:59:50,375
What I think is new is that we can see
the fingerprints and we can show it.


731
01:59:50,541 --> 01:59:54,708
I think that that points to something
that I would say might be new territory,


732
01:59:54,875 --> 01:59:58,375
which is a private company
manufacturing and selling


733
01:59:58,541 --> 02:00:01,541
nation state-level cyberweapons.


734
02:00:07,791 --> 02:00:13,708
There is something of a viral logic
about this form of targeting


735
02:00:13,875 --> 02:00:17,500
in a way that it spreads
between family members


736
02:00:17,833 --> 02:00:21,375
but also the people
that investigate those crimes.


737
02:00:21,625 --> 02:00:24,916
If we know that X activist
was targeted on this date


738
02:00:25,083 --> 02:00:28,750
and then immediately following that,
something physical happened...


739
02:00:28,916 --> 02:00:32,125
Like if one of those threads
could be drawn through the database.


740
02:00:32,291 --> 02:00:35,375
This is the underlying logic
of this investigation...


741
02:00:35,666 --> 02:00:38,583
Digital vs. physical violence.
So let’s start with that.


742
02:00:38,750 --> 02:00:44,666
Look at the same pattern, across in
when and how does it happen in Mexico,


743
02:00:44,833 --> 02:00:49,041
when and how does this kind of
relation happen in the UAE,


744
02:00:49,208 --> 02:00:53,333
when it happens in Rwanda, in India,
if you have data about that,


745
02:00:53,500 --> 02:00:58,666
and find a way to move between
one field of operation to the other.


746
02:00:59,250 --> 02:01:04,458
All the information is there, but this is
now the mobilization of the data.


747
02:01:04,625 --> 02:01:08,625
It is not now about simply plotting
because this material is being plotted,


748
02:01:08,875 --> 02:01:13,250
political events are there plotted on
a timeline, there are physical events...


749
02:01:13,458 --> 02:01:18,625
It's now you have all the strings,
and now you start playing music on them.


750
02:01:18,791 --> 02:01:21,166
You go like,
"I’m just gonna use these notes."


751
02:01:21,333 --> 02:01:23,916
Ta, ta, ta, ta... is targeting.


752
02:01:27,125 --> 02:01:29,041
I’m gonna zoom in a little.


753
02:01:29,333 --> 02:01:33,916
So if we go into the front view,
here we have the field of Mexico...


754
02:01:34,416 --> 02:01:38,291
So, from on top,
it appears just as a line...


755
02:01:39,541 --> 02:01:42,916
collapsed into a single field.
So this is Mexico.


756
02:01:43,458 --> 02:01:46,500
But then if we view it
from the side,


757
02:01:46,666 --> 02:01:50,083
we see all of the targets,
all the people who've been targeted


758
02:01:50,250 --> 02:01:53,166
by NSO software inside Mexico.


759
02:01:53,375 --> 02:01:55,750
So when we're seeing it
in 3D, for example,


760
02:01:55,916 --> 02:02:00,291
we can see that some targets
are operating in multiple fields,


761
02:02:00,500 --> 02:02:06,583
for example, Mazen and Citizen Lab operate
in Mexico, Saudi Arabia and UAE.


762
02:02:07,000 --> 02:02:10,875
Our current database includes
every event that we have


763
02:02:11,041 --> 02:02:14,083
which is an attack that's been
performed using NSO software.


764
02:02:14,250 --> 02:02:17,750
And that database lists
all the people that have been targeted by


765
02:02:17,916 --> 02:02:22,541
the state that has bought NSO software
and is doing the targeting.


766
02:02:29,250 --> 02:02:33,500
<I>I remember the moment when the project
<I>gained urgency in my mind,


767
02:02:33,666 --> 02:02:35,708
<I>and that was with Fleming.


768
02:02:36,083 --> 02:02:40,916
<I>This is NSO's first
<I>so-called "civilian product."


769
02:02:41,208 --> 02:02:44,791
<I>It was described as a kind of contact
<I>tracing tool to monitor both


770
02:02:44,958 --> 02:02:47,125
<I>those who had caught Covid-19,


771
02:02:47,375 --> 02:02:51,291
<I>but also those that
<I>they had come into contact with.


772
02:02:51,541 --> 02:02:55,750
<I>It was reported that Fleming
<I>had been marketed to


773
02:02:55,916 --> 02:02:59,125
<I>dozens of health ministries
<I>around the world.


774
02:03:01,000 --> 02:03:03,500
<I>So, it was a set of alarm bells
<I>in my mind


775
02:03:03,666 --> 02:03:06,250
<I>that the same
<I>cyber weapons manufacturer


776
02:03:06,416 --> 02:03:10,000
<I>that had targeted former collaborators
<I>of Forensic Architecture,


777
02:03:10,166 --> 02:03:13,083
<I>targeted my own personal friends
<I>and colleagues,


778
02:03:13,250 --> 02:03:18,708
<I>and these are all human rights activists,
<I>researchers, lawyers, journalists,


779
02:03:18,916 --> 02:03:22,583
<I>this same company is now
<I>offering its services


780
02:03:22,750 --> 02:03:26,583
<I>to deal with private health details.


781
02:03:27,083 --> 02:03:29,583
<I>NSO Fleming database,
<I>Saudi Arabia


782
02:03:29,833 --> 02:03:34,208
When I first started working
on these cases,


783
02:03:34,500 --> 02:03:39,750
it was clear that
we’re dealing with a company that


784
02:03:39,916 --> 02:03:42,791
does not have a lot of respect
for human rights.


785
02:03:42,958 --> 02:03:46,083
Also if you look at
the reporting about the company,


786
02:03:46,250 --> 02:03:47,708
and the value of the company,


787
02:03:48,000 --> 02:03:51,958
the money that’s involved, we’re talking
about hundreds of millions of dollars.


788
02:03:52,125 --> 02:03:56,875
Of course a lot of the people in Mexico
that you represent have lost their jobs,


789
02:03:57,041 --> 02:03:59,708
they’ve had their offices broken into,


790
02:03:59,875 --> 02:04:02,000
they’ve had their partners killed.


791
02:04:02,166 --> 02:04:06,125
And so the state kills
43 students in Ayotzinapa,


792
02:04:06,291 --> 02:04:08,791
people report on it, they get hacked,


793
02:04:08,958 --> 02:04:11,875
then their lawyers get targeted,
and so on.


794
02:04:12,083 --> 02:04:16,541
And so a lot of what we’re seeing
along the way is this escalation,


795
02:04:16,708 --> 02:04:19,000
this rise in the way
that people are targeted.


796
02:04:19,250 --> 02:04:21,625
Not only they would gather
information,


797
02:04:21,791 --> 02:04:24,166
but also they can send messages.


798
02:04:24,750 --> 02:04:30,708
So they can start managing
your relationship with people


799
02:04:30,875 --> 02:04:34,250
by sending them
all kind of information,


800
02:04:34,416 --> 02:04:37,083
messages that supposedly
come from you.


801
02:04:37,291 --> 02:04:42,291
It’s like somebody's sitting
in your mind.


802
02:04:46,833 --> 02:04:52,375
<I>The NSO group stands for the 3 persons
<I>that founded the company in 2010.


803
02:04:52,541 --> 02:04:57,708
<I>Shalev Hulio is the S in NSO and
<I>perhaps the more well-known of the three.


804
02:04:57,875 --> 02:05:00,500
<I>He’s not exactly a technical wizard.


805
02:05:00,666 --> 02:05:03,333
<I>He did not serve in Unit 8200,


806
02:05:03,583 --> 02:05:06,333
<I>which is the intelligence unit
<I>in the Israeli Army.


807
02:05:06,500 --> 02:05:08,458
<I>But Shalev Hulio did serve
<I>in the Israeli Army,


808
02:05:08,625 --> 02:05:09,666
<I>and as a commander,


809
02:05:09,833 --> 02:05:13,666
<I>he was involved in a series of
<I>operations in the occupied West Bank.


810
02:05:13,833 --> 02:05:18,541
<I>One of his operations was during
<I>the height of the Second Intifada in 2002,


811
02:05:18,708 --> 02:05:21,416
<I>which is Operation Defensive Shield.


812
02:05:21,583 --> 02:05:26,458
<I>This extremely destructive operation
<I>involved sending soldiers


813
02:05:26,625 --> 02:05:29,958
<I>straight through the walls
<I>of Palestinian homes, so


814
02:05:30,166 --> 02:05:33,291
<I>instead of fighting their way
<I>through the alleyways of the camp,


815
02:05:33,458 --> 02:05:38,583
<I>Israeli soldiers would smash their way
<I>through civilian homes to clear a path,


816
02:05:38,750 --> 02:05:41,708
<I>blasting their way
<I>through the built environment,


817
02:05:41,916 --> 02:05:45,791
<I>and mobilizing people’s homes
<I>against their bodies.


818
02:05:50,208 --> 02:05:53,750
<I>An anonymous source
<I>agrees to speak with us.


819
02:05:53,916 --> 02:05:57,375
<I>Do you currently...? Do you hold
<I>a security clearance right now?


820
02:05:57,541 --> 02:06:00,958
<I>Nope, I’ve been kind of
<I>this whistleblower guy in the past.


821
02:06:01,125 --> 02:06:06,208
<I>My interests lie in helping to get
<I>whatever should be public, public.


822
02:06:07,833 --> 02:06:10,041
<I>So you can think of me as a


823
02:06:10,208 --> 02:06:13,041
<I>solutions executive
<I>with a software company.


824
02:06:13,250 --> 02:06:16,208
<I>What it boiled down to was that
<I>we had access to the networks,


825
02:06:16,375 --> 02:06:18,666
<I>and NSO was looking for access.


826
02:06:18,833 --> 02:06:23,708
<I>So the access to the network would be...
<I>They would get a global title.


827
02:06:23,875 --> 02:06:26,666
<I>They would have a piece of software
<I>into it that would have access


828
02:06:26,833 --> 02:06:28,916
<I>to the live operator network.


829
02:06:29,666 --> 02:06:31,166
<I>This was a conference call.


830
02:06:31,333 --> 02:06:34,500
<I>They were very hesitant
<I>to share their names.


831
02:06:34,666 --> 02:06:36,916
<I>I took their names
<I>and email addresses


832
02:06:37,083 --> 02:06:39,625
<I>just in the event
<I>something bad were to happen.


833
02:06:39,791 --> 02:06:42,125
<I>So I met with all these people...


834
02:06:42,291 --> 02:06:45,250
<I>Omri, Eddy Shalev, Eran Gorev...


835
02:06:46,500 --> 02:06:47,750
Wow.


836
02:06:48,250 --> 02:06:51,166
<I>There’s a tremendous amount
<I>of value in that,


837
02:06:51,333 --> 02:06:54,416
<I>to get access to
<I>operator signaling networks.


838
02:06:54,583 --> 02:06:56,583
<I>It made me very uncomfortable.


839
02:06:57,708 --> 02:07:00,833
I did reach out to the FBI,
multiple times...


840
02:07:01,000 --> 02:07:03,875
Do you know what happened
with that information?


841
02:07:04,041 --> 02:07:07,541
I don't know,
it was very silent after that.


842
02:07:08,041 --> 02:07:12,541
Ok, this is...? It’s gonna take
a second to process this.


843
02:07:12,708 --> 02:07:15,625
Are you in a position where
you could provide access globally


844
02:07:15,791 --> 02:07:18,458
or just in the US
or in what countries?


845
02:07:19,666 --> 02:07:20,666
<I>Globally.


846
02:07:20,875 --> 02:07:23,875
<I>In July, I had a meeting
<I>with some executives at the NSO Group


847
02:07:24,041 --> 02:07:26,958
<I>who provides black box spyware
<I>for global governments...


848
02:07:27,125 --> 02:07:30,333
<I>They requested access
<I>to mobile networks via our software


849
02:07:30,541 --> 02:07:32,875
<I>and offered cash payments...


850
02:07:43,958 --> 02:07:45,583
<I>Hey, it's me.


851
02:07:45,833 --> 02:07:48,333
<I>So I’m having all these flashbacks.


852
02:07:48,541 --> 02:07:53,791
<I>The first time Snowden contacted me,
<I>I was sitting here, I was in this room.


853
02:07:55,500 --> 02:07:58,083
<I>There was a kind of demarcation


854
02:07:58,250 --> 02:08:01,166
<I>of my life, before and after.


855
02:08:01,666 --> 02:08:05,208
<I>Those I could trust, and then people
<I>who enter my life after,


856
02:08:05,375 --> 02:08:07,875
<I>who are always in question, right?


857
02:08:09,250 --> 02:08:11,583
<I>And even the people
<I>you knew before


858
02:08:11,750 --> 02:08:13,541
<I>can always be flipped.


859
02:08:16,666 --> 02:08:18,000
<I>And so...


860
02:08:19,166 --> 02:08:24,250
<I>Listening to people tell this story
<I>about this kind of surveillance...


861
02:08:24,416 --> 02:08:27,708
<I>It just hits home, right?
<I>It's super unnerving.


862
02:08:33,083 --> 02:08:38,208
<I>I escaped from my country
<I>under the risk of being arrested.


863
02:08:38,875 --> 02:08:43,541
<I>The field that I'm focusing on,
<I>it's a very sensitive field.


864
02:08:43,708 --> 02:08:49,625
<I>I'm trying to record
<I>the violations of human rights,


865
02:08:49,791 --> 02:08:52,916
<I>how they treat prisoners, detainees,


866
02:08:53,083 --> 02:08:54,916
<I>if they were tortured...


867
02:08:55,125 --> 02:08:58,541
<I>And my father,
<I>he is one of the detainees so


868
02:08:58,791 --> 02:09:02,666
<I>I want to speak about him,
<I>and raise his case.


869
02:09:02,833 --> 02:09:06,291
<I>And I'm his daughter,
<I>I have to do something.


870
02:09:06,500 --> 02:09:10,083
I have to tell you,
as you're talking I'm trying to...


871
02:09:10,250 --> 02:09:15,666
Laura has definitely her experiences
with being targeted and surveilled.


872
02:09:15,833 --> 02:09:20,291
My experiences
with state violence are...


873
02:09:20,583 --> 02:09:22,875
have so far been more physical.


874
02:09:23,166 --> 02:09:26,083
In my family,
we left Iran when we were young,


875
02:09:26,250 --> 02:09:29,416
and I'm just thinking that
we left for political reasons,


876
02:09:29,583 --> 02:09:31,625
my parents were both
political prisoners,


877
02:09:31,791 --> 02:09:34,500
and when we came here,
we continued our politics...


878
02:09:34,666 --> 02:09:39,125
We would go to protest every week
with other Iranian, Afghan...


879
02:09:39,500 --> 02:09:42,041
Kurdish refugees.


880
02:09:42,333 --> 02:09:46,458
My mother was part of the International
Federation of Iranian Refugees,


881
02:09:46,625 --> 02:09:49,875
and she did it here because
she was away from Iran.


882
02:09:50,125 --> 02:09:52,333
If she were to do that today,


883
02:09:52,500 --> 02:09:58,083
the Islamic Republic would be able
to reach out and touch her, so to speak,


884
02:09:58,291 --> 02:10:02,208
in the way that your home government
is reaching out and touching you.


885
02:10:02,458 --> 02:10:07,083
It's a completely different
ball game for activists,


886
02:10:07,333 --> 02:10:11,083
and especially activists
coming from places where...


887
02:10:12,333 --> 02:10:16,250
there is much more
of a physical cost in resisting.


888
02:10:24,875 --> 02:10:27,750
<I>BLM protest, June 19


889
02:10:58,500 --> 02:11:01,666
The link between digital violence
and physical violence becomes


890
02:11:01,833 --> 02:11:05,208
the most clear when we look at
the cases from Saudi Arabia.


891
02:11:13,708 --> 02:11:16,916
He's writing articles,
he's doing columns, he's giving talks.


892
02:11:17,083 --> 02:11:20,083
Then Omar Abdulazziz
and Khashoggi communicate,


893
02:11:20,250 --> 02:11:21,500
and right after that,


894
02:11:21,666 --> 02:11:23,541
Abdulazziz gets targeted digitally.


895
02:11:23,833 --> 02:11:26,958
They communicated almost
on a daily basis.


896
02:11:27,125 --> 02:11:32,416
That means that all the messages
they exchanged were there to be hacked


897
02:11:32,583 --> 02:11:35,125
because they were all saved
on the WhatsApp account.


898
02:11:35,291 --> 02:11:39,791
That was a treasure that
the government of Saudi Arabia found


899
02:11:39,958 --> 02:11:45,250
because they just discovered
the nature of the relationship


900
02:11:45,416 --> 02:11:48,208
between two big dissidents.


901
02:11:50,041 --> 02:11:56,000
One month before they killed him,
he sent me a private message on Twitter


902
02:11:56,416 --> 02:12:00,041
when I was attacked by
Saudi bots and trolls.


903
02:12:00,458 --> 02:12:03,625
He told me, "Ghada, block them,


904
02:12:03,791 --> 02:12:06,500
"ignore them, and don’t get upset.


905
02:12:06,666 --> 02:12:08,750
"Just block them like I do."


906
02:12:11,291 --> 02:12:16,166
And every day, the more we discovered
what happened to him and how he was...


907
02:12:16,333 --> 02:12:19,500
how he went in,
how they told him to sit down,


908
02:12:19,666 --> 02:12:22,000
how they came with the saw,


909
02:12:22,166 --> 02:12:25,416
how...when they started to…


910
02:12:29,958 --> 02:12:33,833
When he couldn’t breathe,
and then, you know, I was…


911
02:12:34,791 --> 02:12:37,916
The details, you know, it was so...


912
02:12:38,666 --> 02:12:42,916
I don’t know how to describe it
actually, it’s a nightmare.


913
02:12:46,208 --> 02:12:49,875
My heart starts beating
and I say, "God, please...


914
02:12:50,458 --> 02:12:55,458
"Let it be quickly, let me read something
that says he didn’t suffer."


915
02:12:55,666 --> 02:12:59,291
He didn’t have time to realize
that he was killed


916
02:12:59,458 --> 02:13:03,166
by his own government,
by his own people.


917
02:13:09,250 --> 02:13:13,583
<I>This assassination was empowered
<I>with Israeli software,


918
02:13:13,750 --> 02:13:17,000
<I>and since then,
<I>Saudi Arabia hasn’t paid a price


919
02:13:17,166 --> 02:13:19,375
<I>because of its alliances
<I>with America,


920
02:13:19,541 --> 02:13:22,541
<I>its investments in Europe,
<I>and so on.


921
02:13:22,791 --> 02:13:27,458
<I>So it's really the dispensability
<I>of our lives.


922
02:13:27,708 --> 02:13:30,583
<I>That's something
<I>that became very clear.


923
02:13:55,125 --> 02:13:56,958
<I>Officially on the record,


924
02:13:57,125 --> 02:14:00,833
<I>NSO isn’t allowed
<I>to operate in the United States.


925
02:14:01,000 --> 02:14:03,791
<I>But it’s attempted
<I>to enter American markets


926
02:14:03,958 --> 02:14:05,791
<I>for the sale of its hacking software


927
02:14:05,958 --> 02:14:08,583
<I>at least a couple of times
<I>that we know about.


928
02:14:08,791 --> 02:14:12,458
<I>In 2016, they reached out
<I>to the San Diego Police Department


929
02:14:12,625 --> 02:14:15,708
<I>to sell a version of Pegasus
<I>called Phantom.


930
02:14:37,625 --> 02:14:41,500
<I>The brochure for Phantom boasts that
<I>it can intercept calls,


931
02:14:41,666 --> 02:14:43,250
<I>do location tracking,


932
02:14:43,416 --> 02:14:45,041
<I>access a person’s messages,


933
02:14:45,208 --> 02:14:47,208
<I>their browsing history,
<I>their social networks,


934
02:14:47,375 --> 02:14:48,875
<I>their contact lists.


935
02:14:49,041 --> 02:14:52,083
<I>So, we now know there is
<I>an appetite from the U.S. police


936
02:14:52,250 --> 02:14:54,208
<I>for these kinds of tools.


937
02:14:55,041 --> 02:14:59,416
<I>But in the context of a pandemic,
<I>where we are seeing rising inequality,


938
02:14:59,583 --> 02:15:02,541
<I>in a context where communities
<I>are protesting en masse


939
02:15:02,708 --> 02:15:04,208
<I>against racialized policing,


940
02:15:04,375 --> 02:15:06,333
<I>it's extremely fucking scary


941
02:15:06,500 --> 02:15:08,875
<I>to think of
<I>what incredible damage


942
02:15:09,041 --> 02:15:11,166
<I>tools like Pegasus and Phantom


943
02:15:11,333 --> 02:15:14,333
<I>can do in the hands
<I>of law enforcement.


944
02:15:19,916 --> 02:15:24,000
<I>NYPD counterterrorism center


945
02:15:27,625 --> 02:15:31,625
<I>In the summer of 2021,
<I>17 news organizations began publishing


946
02:15:31,833 --> 02:15:36,166
<I>the "Pegasus Project," exposing
<I>thousands of new targets including


947
02:15:36,333 --> 02:15:39,625
<I>journalists, dissidents and heads of state.


948
02:15:39,791 --> 02:15:45,333
<I>The reporting revealed new instances
<I>of journalists and activists selected for


949
02:15:45,500 --> 02:15:49,250
<I>targeting shortly before
<I>being tortured or murdered.


950
02:15:49,416 --> 02:15:52,541
<I>NSO denies the claims made in this film.
